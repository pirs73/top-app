/* eslint-disable @next/next/no-page-custom-font */
import Head from 'next/head';
import { AppProps } from 'next/app';
import '../styles/globals.css';

function MyApp({ Component, pageProps }: AppProps): JSX.Element {
  return (
    <>
      <Head>
        <title>Top App</title>
      </Head>
      <Component {...pageProps} />
    </>
  );
}

export default MyApp;
