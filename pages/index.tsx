/* eslint-disable @typescript-eslint/no-unused-vars */
import { GetStaticProps } from 'next';
import { useState } from 'react';
import axios from 'axios';
import { Button, Htag, Input, P, Rating, Tag, Textarea } from '../components';
import { withLayout } from '../layout/Layout';
import { MenuItem } from '../interfaces/menu.interface';
interface HomeProps extends Record<string, unknown> {
  menu: MenuItem[];
  firstCategory: number;
}

const Home = ({ menu }: HomeProps): JSX.Element => {
  const [rating, setRating] = useState<number>(4);

  return (
    <>
      <Htag tag="h1">counter</Htag>
      <Button appearance="primary" arrow="right">
        Button
      </Button>
      <br />
      <Button appearance="ghost" arrow="down">
        Button
      </Button>
      <P size="l">paragraph L</P>
      <P>paragraph M</P>
      <P size="s">paragraph S</P>
      <Tag size="s">small</Tag>
      <Tag size="m" color="red">
        medium
      </Tag>
      <Tag size="m" color="green">
        green
      </Tag>
      <Tag size="s" color="primary" href="//google.com/">
        google
      </Tag>
      <Rating rating={rating} isEditable setRating={setRating} />
      <Input placeholder="test" />
      <Textarea placeholder="test area" />
    </>
  );
};

export const getStaticProps: GetStaticProps<HomeProps> = async () => {
  const firstCategory = 0;
  const { data: menu } = await axios.post<MenuItem[]>(
    process.env.NEXT_PUBLIC_DOMAIN + '/api/top-page/find',
    { firstCategory },
  );
  return {
    props: {
      menu,
      firstCategory,
    },
  };
};

export default withLayout(Home);
