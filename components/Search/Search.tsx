import { KeyboardEvent, useState } from 'react';
import { useRouter } from 'next/router';
import cn from 'classnames';
import { Input } from '../Input/Input';
import { Button } from '../Button/Button';
import GlassIcon from './icons/glass.svg';
import { SearchProps } from './Search.props';
import styles from './Search.module.css';

const Search = ({ className, ...props }: SearchProps): JSX.Element => {
  const [search, setSearch] = useState<string>('');
  const router = useRouter();

  const goToSearch = () => {
    router.push({
      pathname: '/search',
      query: {
        q: search,
      },
    });
  };

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const handleKeyDown = (e: KeyboardEvent) => {
    if (e.key === 'Enter') {
      goToSearch();
    }
  };

  return (
    <div className={cn(styles.search, className)} {...props}>
      <Input
        className={styles.input}
        placeholder="Поиск..."
        value={search}
        onChange={(e) => setSearch(e.target.value)}
        onKeyDown={handleKeyDown}
      />
      <Button
        appearance="primary"
        className={styles.button}
        // eslint-disable-next-line @typescript-eslint/no-empty-function
        onClick={goToSearch}
      >
        <GlassIcon />
      </Button>
    </div>
  );
};

export { Search };
